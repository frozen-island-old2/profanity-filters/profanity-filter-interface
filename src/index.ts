export default interface ProfanityFilter{
    filter(unsafeText: string): FilterOutput;
}

export interface FilterOutput {
    message: string,
    flaged: boolean
}
